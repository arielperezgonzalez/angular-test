# Prueba de desarrollo en Angular (3 hrs para completar):

### Cree una aplicación desde cero de Lista de Contactos utilizando: 
* TypeScript
* Angular 6+
* Angular Material
* La interfaz de línea de comandos de Angular.

* Escriba código usando el paradigma de programación reactiva con RxJS y Observables.

### La funcionalidad mínima debe incluir:
* Crear un contacto (nombre, apellido, numero de telefono)
* Listar todos los contactos
* Modificar un contacto
* Eliminar un contacto

#### No se requiere autenticación

#### No se requiere base de datos
