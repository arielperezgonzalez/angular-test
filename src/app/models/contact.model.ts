export default class Contact {
  name: string;
  lastname: string;
  phone: string;
  id : any;

  constructor(id: any, name: string, lastname: string, phone: string) { 
  	this.id = id;
  	this.name = name;
  	this.lastname = lastname;
  	this.phone = phone;
  }
}
