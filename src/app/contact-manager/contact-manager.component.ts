import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import Contact from '../models/contact.model';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-contact-manager',
  templateUrl: './contact-manager.component.html',
  styleUrls: ['./contact-manager.component.css']
})
export class ContactManagerComponent implements OnInit {
  isEditing: boolean = false;
  contacts: Contact[] = [];
  contactsObservable: Observable<Contact[]>;

  constructor(private dataService: DataService) { 

    this.contactsObservable = this.dataService.getContacts();
    this.contactsObservable.subscribe((contacts: Contact[]) => {
        this.contacts = contacts;
    });
  }

  ngOnInit()
  {

  }

  addContact() : void
  {
  	this.isEditing = true;
  	this.dataService.addContact(new Contact(0, "", "", ""));
  }

}
