import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import Contact from '../models/contact.model';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { ConfirmDialogModel, ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  @Input() contact;
  name : FormControl = new FormControl();
  lastname : FormControl = new FormControl();
  phone : FormControl = new FormControl("", [Validators.pattern("[0-9]{3}-[0-9]{3}-[0-9]{4}")]);
  //contactForm: FormGroup;

  isEditing : boolean = true;
  isAdding : boolean = true;
  constructor(private dataService: DataService, private dialog: MatDialog, private snackBar: MatSnackBar) { 

  }

  ngOnInit()
  {
  	this.name.setValue(this.contact.name);
  	this.lastname.setValue(this.contact.lastname);
  	this.phone.setValue(this.contact.phone);
  }

  onEdit() : void {
  	this.isEditing = true;
  	this.onStateChanged();
  }

  onDelete() : void {

    const message = "Are you sure you want to delete the contact?";
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });
 
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult)
      {
        this.dataService.deleteContact(this.contact);
      }
    });
  }

  onSave() : void{
    if ((this.name.value === "") || (this.lastname.value === "") || (this.lastname.value === undefined) || (this.lastname.value === undefined)) 
    {
      this.snackBar.open('You must enter fields: Name and LastName', null, {duration: 3000});
      return;
    }

    if (this.phone.invalid)
    {
       this.snackBar.open('Phone number is invalid: please enter your phone with this format: 999-999-9999', null, {duration: 3000});
       return; 
    }


  	this.contact.name = this.name.value;
  	this.contact.lastname = this.lastname.value;
  	this.contact.phone = this.phone.value;
  		
  	this.isEditing = false;
  	this.isAdding = false;

  	this.dataService.editContact(this.contact);
  	this.onStateChanged();
  }

  onCancel() : void {
  	this.isEditing = false;

  	if (this.isAdding)
  		this.dataService.deleteContact(this.contact);
  }

  private onStateChanged() : void
  {
  	if (this.isEditing)
  	{
  		this.name.enable();
  		this.lastname.enable();
  		this.phone.enable();
  	}
  	else
  	{
  		this.name.disable();
  		this.lastname.disable();
  		this.phone.disable();
  	}
  }

}
