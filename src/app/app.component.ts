import { Component } from '@angular/core';
import { DataService } from './services/data.service';
import { MatDialog } from '@angular/material';
import { ConfirmDialogModel, ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title : string = 'Angular Dev Test';

  constructor(private dataService: DataService, private dialog: MatDialog) { 
    
  }

  onClear() : void
  {

  	const message = `Are you sure you want to delete all the contacts?`;
 
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
 
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });
 
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult)
      {
        this.dataService.clearData();
      }
    });
  }
}
