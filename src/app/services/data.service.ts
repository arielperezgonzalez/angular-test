import { Injectable } from '@angular/core';
import Contact from '../models/contact.model';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  counter : any = 0;
  contacts: Contact[] = [];

  getContacts(): Observable<Contact[]> {
    return of<Contact[]>(this.contacts);
  }

  addContact(contact: Contact) : void {
    contact.id = this.counter++;
    this.contacts.unshift(contact);
  }

  editContact(contact: Contact) : void{
    this.contacts[this.contacts.findIndex(function(i){
        return i.id === contact.id;
       })] = contact;
  }

  deleteContact(contact: Contact) : void {
    this.contacts.splice(
      this.contacts.findIndex(function(i){
        return i.id === contact.id;
       }), 
     1);
  }

  clearData() : void
  {
    while (this.contacts.length) {
      this.contacts.pop();
    }
  }
}
